import CenteredPage from '../../containers/CenteredPage/CenteredPage';
import classes from './Rules.module.css';

const Rules: React.FC = (): JSX.Element => {
  return (
    <CenteredPage>
      <h1 className={classes.header}>Rules</h1>
      <ul className={classes.list}>
        <li>Applicants must be 16+</li>
        <li>Be kind and respectful</li>
        <li>No malicious griefing of other players</li>
        <li>Don't steal from others</li>
        <li>No harassment, racism, or other derogatory means of hate speech</li>
        <li>Any exploitive clients or X-Ray is strictly prohibited</li>
        <li>No modifications other than those permitted by staff</li>
      </ul>
    </CenteredPage>
  );
};

export default Rules;
