import { useEffect, useState } from 'react';
import CenteredPage from '../../containers/CenteredPage/CenteredPage';
import configuredFirebase from '../../helpers/firebase';
import classes from './Plugins.module.css';

const Plugins: React.FC = (): JSX.Element => {
  const [plugins, setPlugins] = useState<JSX.Element[]>();
  const [datapacks, setDatapacks] = useState<JSX.Element[]>();
  useEffect(() => {
    configuredFirebase
      .database()
      .ref('website-data/Plugins and Datapacks')
      .get()
      .then((snapshot) => {
        const data = snapshot.toJSON();
        const datapackDisplay: JSX.Element[] = [];
        const pluginDisplay: JSX.Element[] = [];
        if (data !== null) {
          Object.entries(data).forEach((el) => {
            if (el[1] !== '') {
              pluginDisplay.push(
                <li>
                  <a href={el[1]}>{el[0]}</a>
                </li>
              );
            } else {
              datapackDisplay.push(<li>{el[0]}</li>);
            }
          });
        }
        setPlugins(pluginDisplay);
        setDatapacks(datapackDisplay);
      });
  }, []);
  return (
    <CenteredPage className={classes.container}>
      <h3 className={classes.header}>Plugins</h3>
      <ul className={classes.list}>{plugins}</ul>
      <h3 className={classes.header}>Datapacks</h3>
      <ul className={classes.list}>{datapacks}</ul>
    </CenteredPage>
  );
};

export default Plugins;
