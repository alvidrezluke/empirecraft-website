import classes from './Leaderboard.module.css';

import Scoreboard from '../../components/Scoreboard/Scoreboard';

const Leaderboard: React.FC = (): JSX.Element => {
  return (
    <>
      <h1 className={classes.header}>Leaderboard</h1>
      <h3 className={classes.description}>
        Click on the buttons to sort the leaderboard.
      </h3>
      <Scoreboard />
    </>
  );
};

export default Leaderboard;
