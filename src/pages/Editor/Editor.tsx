import { MouseEventHandler, useEffect, useRef, useState } from 'react';
import configuredFirebase from '../../helpers/firebase';
import classes from './Editor.module.css';

const Editor: React.FC = (): JSX.Element => {
  const user = configuredFirebase.auth().currentUser;
  const formRef = useRef<HTMLFormElement>(null);
  /*console.log(
    configuredFirebase
      .database()
      .ref('website-data')
      .update({
        'Plugins and Datapacks': {
          CoreProtect: 'https://www.spigotmc.org/resources/coreprotect.8631/',
          Dynmap: 'https://dev.bukkit.org/projects/dynmap',
          'Plasmo Voice Server':
            'https://www.spigotmc.org/resources/plasmo-voice-server.91064/',
          EssentialsX: 'https://www.spigotmc.org/resources/essentialsx.9089/',
          'EssentialsX Chat':
            'https://www.spigotmc.org/resources/essentialsx.9089/',
          OpenInv: 'https://dev.bukkit.org/projects/openinv',
          'Inventory Rollback':
            'https://www.spigotmc.org/resources/inventory-rollback.48074/',
          LuckPerms: 'https://www.spigotmc.org/resources/luckperms.28140/',
          ProtocolLib: 'https://www.spigotmc.org/resources/protocollib.1997/',
          Vault: 'https://www.spigotmc.org/resources/vault.34315/',
          'AFK Display': '',
          'Armor Statues': '',
          'Double Shulker Shells': '',
          'Dragon Drops': '',
          'More Mob Heads': '',
          'Player Head Drops': '',
          'Silence Mobs': '',
          CraftingTweaks: '',
          'Wandering Trades': '',
          'Tag (minigame)': '',
        },
      })
  );*/

  const addNewField: MouseEventHandler<HTMLButtonElement> = (event) => {
    event.preventDefault();
    const target: Element = event.target as unknown as Element;
    if (formRef.current !== null) {
      formRef.current.insertBefore(document.createElement('input'), target);
    }
  };

  const [formContent, setFormContent] = useState<JSX.Element[]>();
  useEffect(() => {
    configuredFirebase
      .database()
      .ref('website-data')
      .get()
      .then((snapshot) => {
        let formData: JSX.Element[] = [];
        const data = snapshot.toJSON();
        if (data !== null) {
          Object.entries(data).forEach((cat) => {
            const subtitle = cat[0];
            if (subtitle !== null) {
              formData.push(<h4 key={subtitle}>{subtitle}</h4>);
            }
            const fields = cat[1];
            if (fields !== null) {
              Object.entries(fields).forEach((content) => {
                let label: string = content[0];
                label = label.replace('&period;', '.');
                const currentValue: string = content[1] as string;
                formData.push(
                  <fieldset>
                    <label htmlFor={label}>{label}: </label>
                    <input id={label} type='text' placeholder={currentValue} />
                  </fieldset>
                );
              });
            }
            //Add a button to add a value
            formData.push(
              <button onClick={addNewField} id={subtitle}>
                +
              </button>
            );
          });
        }
        setFormContent(formData);
      });
  }, []);
  const updateconfiguredFirebase = (): void => {};

  return (
    <>
      <h1>Editor</h1>
      <h3>Welcome{user?.displayName ? ' ' + user?.displayName : null}!</h3>
      <form
        onSubmit={updateconfiguredFirebase}
        className={classes.form}
        ref={formRef}
      >
        {formContent}
      </form>
    </>
  );
};

export default Editor;
