import classes from './ContentCreators.module.css';
import Creator from '../../components/Creator/Creator';
import { useEffect, useState } from 'react';
import configuredFirebase from '../../helpers/firebase';
import CenteredPage from '../../containers/CenteredPage/CenteredPage';

const ContentCreators: React.FC = (): JSX.Element => {
  const [creators, setCreators] = useState<JSX.Element[]>();
  useEffect(() => {
    configuredFirebase
      .database()
      .ref('/website-data/Content Creators')
      .get()
      .then((snapshot) => {
        const data = snapshot.toJSON();
        const parsedCreators: JSX.Element[] = [];
        if (data !== null) {
          Object.entries(data).forEach((creator) => {
            let platforms: string[][] = [];
            Object.entries(creator[1]).forEach((platform) => {
              if (platform[0] !== 'profileImg') {
                platforms.push([platform[0], platform[1] as string]);
              }
            });
            parsedCreators.push(
              <Creator
                username={creator[0]}
                profileImg={creator[1]['profileImg']}
                platforms={platforms}
                key={creator[0]}
              />
            );
          });
        }
        setCreators(parsedCreators);
      });
  }, []);
  return (
    <CenteredPage className={classes['content-creators__container']}>
      {creators}
    </CenteredPage>
  );
};

export default ContentCreators;
