import React, { FormEvent, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import CenteredPage from '../../containers/CenteredPage/CenteredPage';
import configuredFirebase from '../../helpers/firebase';
import classes from './Login.module.css';

interface ILoginOwnProps {
  location?: {
    state?: {
      origin: string;
    };
  };
}

const Login: React.FC<ILoginOwnProps> = ({ location }): JSX.Element => {
  const emailRef = useRef<HTMLInputElement>(null);
  const passwordRef = useRef<HTMLInputElement>(null);
  const rememberRef = useRef<HTMLInputElement>(null);

  const [error, setError] = useState(<></>);

  const history = useHistory();

  const success = () => {
    console.log('success');
    const origin = location?.state?.origin;
    if (origin !== null && origin !== undefined) {
      history.push(origin);
    } else {
      history.push('/');
    }
  };
  const failure = (err: string) => {
    setError(<p className={classes.error}>{'Error logging in: ' + err}</p>);
  };

  const login = (event: FormEvent) => {
    event.preventDefault();
    if (
      emailRef.current !== null &&
      passwordRef.current !== null &&
      rememberRef.current !== null
    ) {
      if (emailRef.current.value.trim() === '') {
        failure('Email can not be empty.');
        return;
      }
      if (passwordRef.current.value === '') {
        failure('Password can not be empty.');
        return;
      }
      configuredFirebase
        .auth()
        .setPersistence(rememberRef.current.checked ? 'local' : 'session')
        .then(() => {
          if (emailRef.current !== null && passwordRef.current) {
            configuredFirebase
              .auth()
              .signInWithEmailAndPassword(
                emailRef.current.value,
                passwordRef.current.value
              )
              .then(() => success())
              .catch((err) => failure(err));
          }
        });
    }
  };
  return (
    <CenteredPage>
      <form onSubmit={login} className={classes.form}>
        <label htmlFor='email'>Email:</label>
        <input id='email' type='email' ref={emailRef} />
        <label htmlFor='password'>Password:</label>
        <input id='password' type='password' ref={passwordRef} />
        <section>
          <label htmlFor='remember'>Remember Me:</label>
          <input
            id='remember'
            type='checkbox'
            ref={rememberRef}
            className={classes['form__checkbox']}
          />
        </section>
        <button type='submit'>Submit</button>
        {error ? error : null}
      </form>
    </CenteredPage>
  );
};

export default Login;
