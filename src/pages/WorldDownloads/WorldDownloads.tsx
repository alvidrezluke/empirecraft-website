import CenteredPage from '../../containers/CenteredPage/CenteredPage';
import classes from './WorldDownloads.module.css';

const WorldDownloads: React.FC = (): JSX.Element => {
  return (
    <CenteredPage>
      <h1 className={classes.header}>World Downloads</h1>
      <h4 className={classes.subheader}>Click on any link below to download</h4>
      <ul className={classes.list}>
        <li>
          <a href='https://www.dropbox.com/sh/auhkg90hggdj8ag/AACkXp42M3YRxT4aQUD-uhDoa/Season%201/EmpireCraftS1.zip?dl=0'>
            Season 1
          </a>
        </li>
        <li>
          <a href='https://www.dropbox.com/sh/auhkg90hggdj8ag/AABoKMO8R3fdknxxKLmISDC2a/Season%202/Pre-1.16%20Files/The%20End%20%28Main%20Island%29.zip?dl=0'>
            Season 2 (Pre-1.16 End Main Island)
          </a>
        </li>
        <li>
          <a href='https://www.dropbox.com/sh/auhkg90hggdj8ag/AAAaca69r70vUE_13FjHOOzsa/Season%202/Pre-1.16%20Files/The%20Nether.zip?dl=0'>
            Season 2 (Pre-1.16 Nether)
          </a>
        </li>
        <li>
          <a href='https://drive.google.com/file/d/1M6yi1AHjsUkuZ0n0NJOXaTt7gYj8FpT9/view'>
            Season 2.5
          </a>
        </li>
        <li>
          <a href='https://drive.google.com/drive/folders/17Dyvb7hJJYX05DBMjHQvZfz4sMqDtqN7?usp=sharing'>
            Season 3
          </a>
        </li>
      </ul>
    </CenteredPage>
  );
};

export default WorldDownloads;
