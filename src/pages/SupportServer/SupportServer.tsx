import CenteredPage from '../../containers/CenteredPage/CenteredPage';
import classes from './SupportServer.module.css';

const SupportServer: React.FC = (): JSX.Element => {
  return (
    <CenteredPage className={classes.container}>
      <h2 className={classes.heading}>
        All donations will go directly into the server hardware/costs. Donations
        are always appreciated, but never required. Thank you!
      </h2>
      <h3 className={classes.link}>
        <a href='https://www.paypal.com/paypalme/HaydenSnowDesigns'>
          Support me on PayPal!
        </a>
      </h3>
      <h3 className={classes.link}>
        <a href='https://www.patreon.com/empirecraftsmp'>
          Support me on Patreon!
        </a>
      </h3>
    </CenteredPage>
  );
};

export default SupportServer;
