import classes from './Information.module.css';
import logo from '../../assets/images/LogoSmall.png';
import CenteredPage from '../../containers/CenteredPage/CenteredPage';
import { useRef } from 'react';

const Information: React.FC = (): JSX.Element => {
  const mainRef = useRef<HTMLElement>(null);
  const creativeRef = useRef<HTMLElement>(null);
  const copyText = (text: string) => {
    const el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };
  const copyMain = () => {
    copyText('play.empirecraftmc.com');
    if (mainRef.current !== null && creativeRef.current !== null) {
      mainRef.current.innerText = ' (Copied!)';
      creativeRef.current.innerText = ' (Click to copy)';
    }
  };
  const copyCreative = () => {
    copyText('149.56.242.156:25641');
    if (mainRef.current !== null && creativeRef.current !== null) {
      mainRef.current.innerText = ' (Click to copy)';
      creativeRef.current.innerText = ' (Copied!)';
    }
  };
  return (
    <CenteredPage>
      <div className={classes['logo-container']}>
        <img src={logo} alt='EmpireCraft' className={classes.logo} />
      </div>
      <section>
        <ul className={classes['ip-list']}>
          <li>
            <strong>Main Server IP:</strong>{' '}
            <button onClick={copyMain} className={classes.ip}>
              play.empirecraftmc.com
            </button>
            <span ref={mainRef}> (Click to copy)</span>
          </li>
          <li>
            <strong>Creative Server IP:</strong>{' '}
            <button onClick={copyCreative} className={classes.ip}>
              149.56.242.156:25641
            </button>
            <span ref={creativeRef}> (Click to copy)</span>
          </li>
        </ul>
        <p className={classes.desc}>Server Location: New York, USA</p>
        <p className={classes.desc}>Seed: -7138710392528135036</p>
        <p className={classes.desc}>Minecraft Version: Java 1.18</p>
      </section>
    </CenteredPage>
  );
};

export default Information;
