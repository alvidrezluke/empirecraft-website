import { Link } from 'react-router-dom';
import classes from './FAQ.module.css';

const FAQ: React.FC = (): JSX.Element => {
  return (
    <main>
      <h1 className={classes['main-header']}>Frequently Asked Questions</h1>
      <h3 className={classes.subheading}>
        How do I apply to join EmpireCraft?
      </h3>
      <p className={classes.answer}>
        To join EmpireCraft, please join our{' '}
        <a href='https://discord.gg/56B3W3k'>Discord Server</a> and send a DM to
        RedHeadJedi. You must be 16 or older to apply!
      </p>
      <h3 className={classes.subheading}>How often does the server reset?</h3>
      <p className={classes.answer}>
        This is usually decided on as a group, but most resets take place after
        a major Minecraft update.
      </p>
      <h3 className={classes.subheading}>How do I use Plasmo Voice?</h3>
      <p className={classes.answer}>
        In order to use our proximity voice chat system, you will need to
        install the Plasmo Voice Client,{' '}
        <a href='https://www.curseforge.com/minecraft/mc-mods/plasmo-voice-client'>
          which can be found here
        </a>
        . This mod runs on both Forge and Fabric, and will automatically connect
        to the server side plugin. This mod is optional, and you will be able to
        logon to the server without it.
      </p>
      <h3 className={classes.subheading}>
        On which level are the nether tunnels located?
      </h3>
      <p className={classes.answer}>
        Most of the nether tunnel network is located on <strong>Y=100</strong>.
      </p>
      <h3 className={classes.subheading}>How can I support the server?</h3>
      <p className={classes.answer}>
        If you would like to support the server, you can donate to me{' '}
        <Link to='/support-server'>here</Link>. All donations will go directly
        into the server hardware/costs. Donations are always appreciated, but
        never required. Thank you!
      </p>
    </main>
  );
};

export default FAQ;
