import classes from './Home.module.css';
//import background from '../../assets/images/Background.png';
import logo from '../../assets/images/LogoSmall.png';
import { useHistory } from 'react-router-dom';
import CtaButton from '../../components/Buttons/CtaButton/CtaButton';

const Home: React.FC = (): JSX.Element => {
  const history = useHistory();
  const redirectFindOutMore = (): void => {
    history.push('/server-information');
  };
  return (
    <main className={classes.main}>
      {/*<img src={background} alt='' className={classes.background} />*/}
      <img src={logo} alt='EmpireCraft' className={classes.logo} />
      <CtaButton onClick={redirectFindOutMore}>Find out more</CtaButton>
      {/*<section className={classes.disclaimer}>
        <h3>This is currently a work in progress.</h3>
        <p>
          If you have any suggestions or issues or just want to see the code
          please visit{' '}
          <a href='https://gitlab.com/alvidrezluke/empirecraft-website'>
            the Gitlab
          </a>{' '}
          or PM Legojedimindtrick on Discord.
          <br /> If you would like to contribute please contact me at{' '}
          <a href='mailto:alvidrezluke@gmail.com'>alvidrezluke@gmail.com</a>.
        </p>
  </section>*/}
    </main>
  );
};

export default Home;
