import { useEffect, useState } from 'react';
import TwitchIcon from '../../assets/images/TwitchGlitchPurple.png';
import YoutubeIcon from '../../assets/images/yt_icon_rgb.png';
import classes from './Creator.module.css';

interface ICreatorOwnProps {
  username: string;
  profileImg: string;
  platforms: string[][];
}

const Creator: React.FC<ICreatorOwnProps> = ({
  username,
  profileImg,
  platforms,
}): JSX.Element => {
  const [displayedPlatforms, setDisplayedPlatforms] = useState<JSX.Element[]>();
  useEffect(() => {
    const parsedPlatforms: JSX.Element[] = [];
    platforms.forEach((platform) => {
      let image: JSX.Element = <></>;
      if (platform[0] === 'Twitch') {
        image = (
          <img src={TwitchIcon} alt='' className={classes['platform-logo']} />
        );
      } else if (platform[0] === 'YouTube') {
        image = (
          <img src={YoutubeIcon} alt='' className={classes['platform-logo']} />
        );
      }
      parsedPlatforms.push(
        <li className={classes.platform}>
          {image}
          <a href={platform[1]}>{platform[0]}</a>
        </li>
      );
    });
    setDisplayedPlatforms(parsedPlatforms);
  }, [platforms]);

  return (
    <section className={classes['creator__container']}>
      <div className={classes['creator__image--container']}>
        <img src={profileImg} alt='' className={classes['creator__image']} />
      </div>
      <h3 className={classes['creator__username']}>{username}</h3>
      <ul className={classes['creator__platforms--container']}>
        {displayedPlatforms}
      </ul>
    </section>
  );
};

export default Creator;
