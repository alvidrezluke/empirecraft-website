import classes from './Scoreboard.module.css';
import configuredFirebase from '../../helpers/firebase';
import React, { MouseEventHandler, useEffect, useRef, useState } from 'react';

const Scoreboard: React.FC = (): JSX.Element => {
  const [tableBody, setTableBody] = useState([
    <React.Fragment key='frag'></React.Fragment>,
  ]);

  const [descending, setDescending] = useState(false);

  useEffect(() => {
    const tableData: JSX.Element[] = [];
    configuredFirebase
      .database()
      .ref('users')
      .get()
      .then((snapshot) => {
        const data = snapshot.toJSON();
        if (data !== null) {
          Object.values(data).forEach((el) => {
            const uuid = el.uuid;
            const username = el.username;
            const mobKills =
              el.stats['minecraft:custom']['minecraft:mob_kills'] !== undefined
                ? el.stats['minecraft:custom']['minecraft:mob_kills']
                : '0';
            const totalMined =
              el.stats['minecraft:custom']['minecraft:total_mined'] !==
              undefined
                ? el.stats['minecraft:custom']['minecraft:total_mined']
                : '0';
            const totalFlown =
              el.stats['minecraft:custom']['minecraft:fly_one_cm'] !== undefined
                ? Math.round(
                    el.stats['minecraft:custom']['minecraft:fly_one_cm'] / 100
                  )
                : '0';
            let playerKills = 0;
            if (
              el.stats['minecraft:custom']['minecraft:player_kills'] !==
              undefined
            ) {
              playerKills =
                el.stats['minecraft:custom']['minecraft:player_kills'];
            }
            let killedByPlayer = 0;
            if (el.stats['minecraft:killed_by'] !== undefined) {
              if (
                el.stats['minecraft:killed_by']['minecraft:player'] !==
                undefined
              ) {
                killedByPlayer =
                  el.stats['minecraft:killed_by']['minecraft:player'];
              }
            }
            const playTime =
              el.stats['minecraft:custom']['minecraft:play_time'] / 72000 !==
              undefined
                ? Math.round(
                    el.stats['minecraft:custom']['minecraft:play_time'] / 7200
                  ) / 10
                : 0;

            tableData.push(
              <tr key={uuid}>
                <td>{username}</td>
                <td>{mobKills}</td>
                <td>{totalMined}</td>
                <td>{totalFlown}</td>
                <td>{playerKills}</td>
                <td>{killedByPlayer}</td>
                <td>{playTime}</td>
              </tr>
            );
          });
          setTableBody(tableData);
        }
      });
  }, []);

  const tableRef = useRef<HTMLTableElement>(null);

  const getTable = (): string[][] => {
    if (tableRef.current !== null) {
      const oldTable: string = tableRef.current.innerHTML
        .replaceAll('</tr>', ' ')
        .replace('</tbody>', '');
      let tableData: string[][] = [];
      const tableRows = oldTable.split('<tbody>')[1].split('<tr>');
      tableRows.forEach((str) => {
        const row = str.replaceAll('</td>', '').split('<td>');
        row.shift();
        if (row !== undefined) {
          tableData.push(row);
        }
      });
      tableData.shift();
      return tableData;
    } else {
      return [[]];
    }
  };

  const sortDataByNumber = (column: number): void => {
    const table = getTable();
    table.sort((a, b) => {
      if (parseInt(a[column]) === parseInt(b[column])) {
        return 0;
      } else if (descending) {
        return parseInt(a[column]) > parseInt(b[column]) ? -1 : 1;
      } else {
        return parseInt(a[column]) < parseInt(b[column]) ? -1 : 1;
      }
    });
    if (descending) {
      setDescending(false);
    } else {
      setDescending(true);
    }
    let sortedTable: JSX.Element[] = [];
    table.forEach((row) => {
      sortedTable.push(
        <tr key={row[0]}>
          <td>{row[0]}</td>
          <td>{row[1]}</td>
          <td>{row[2]}</td>
          <td>{row[3]}</td>
          <td>{row[4]}</td>
          <td>{row[5]}</td>
          <td>{row[6]}</td>
        </tr>
      );
    });
    setTableBody(sortedTable);
  };

  const sortDataByUsername = (): void => {
    const table = getTable();
    table.sort((a, b) => {
      if (a[0].toLowerCase() === b[0].toLowerCase()) {
        return 0;
      } else if (descending) {
        return a[0].toLowerCase() > b[0].toLowerCase() ? -1 : 1;
      } else {
        return a[0].toLowerCase() < b[0].toLowerCase() ? -1 : 1;
      }
    });
    if (descending) {
      setDescending(false);
    } else {
      setDescending(true);
    }
    let sortedTable: JSX.Element[] = [];
    table.forEach((row) => {
      sortedTable.push(
        <tr key={row[0]}>
          <td>{row[0]}</td>
          <td>{row[1]}</td>
          <td>{row[2]}</td>
          <td>{row[3]}</td>
          <td>{row[4]}</td>
          <td>{row[5]}</td>
          <td>{row[6]}</td>
        </tr>
      );
    });
    setTableBody(sortedTable);
  };

  const handleClick: MouseEventHandler = (event: React.MouseEvent): void => {
    if (event.target !== null) {
      switch (event.currentTarget.innerHTML) {
        case 'Username':
          sortDataByUsername();
          break;
        case 'Mobs killed':
          sortDataByNumber(1);
          break;
        case 'Total blocks mined':
          sortDataByNumber(2);
          break;
        case 'Distance flown (blocks)':
          sortDataByNumber(3);
          break;
        case 'Number of players killed':
          sortDataByNumber(4);
          break;
        case 'Number of times killed by player':
          sortDataByNumber(5);
          break;
        case 'Total play time (h)':
          sortDataByNumber(6);
          break;
      }
    }
  };

  return (
    <table className={classes.table} ref={tableRef}>
      <thead>
        <tr>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Username
            </button>
          </th>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Mobs killed
            </button>
          </th>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Total blocks mined
            </button>
          </th>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Distance flown (blocks)
            </button>
          </th>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Number of players killed
            </button>
          </th>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Number of times killed by player
            </button>
          </th>
          <th>
            <button onClick={handleClick} className={classes['header__btn']}>
              Total play time (h)
            </button>
          </th>
        </tr>
      </thead>
      <tbody>{tableBody}</tbody>
    </table>
  );
};

export default Scoreboard;
