import classes from './Ctabutton.module.css';

interface ICtaButtonOwnProps {
  onClick: () => void;
}

const CtaButton: React.FC<ICtaButtonOwnProps> = ({
  children,
  onClick,
}): JSX.Element => {
  return (
    <button className={classes['cta-btn']} onClick={onClick}>
      {children}
    </button>
  );
};

export default CtaButton;
