import classes from './NavigationDropdown.module.css';
import { ReactComponent as Arrow } from './down-arrow.svg';

interface INavigationDropdownOwnProps {
  title: string;
}

const NavigationDropdown: React.FC<INavigationDropdownOwnProps> = ({
  children,
  title,
}): JSX.Element => {
  return (
    <div className={classes.dropdown}>
      <p>
        {title}
        <Arrow className={classes.arrow} />
      </p>
      <ul>{children}</ul>
    </div>
  );
};

export default NavigationDropdown;
