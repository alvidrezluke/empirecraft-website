import { Link } from 'react-router-dom';
import classes from './Navigation.module.css';
import logo from '../../assets/images/NavbarLogo.png';
import NavigationDropdown from './NavigationDropdown/NavigationDropdown';
import { useRef } from 'react';

const Navigation: React.FC = (): JSX.Element => {
  const checkboxRef = useRef<HTMLInputElement>(null);
  const handleNavigatePage = () => {
    if (checkboxRef.current !== null) {
      const currentValue = checkboxRef.current.checked;
      checkboxRef.current.checked = !currentValue;
    }
  };
  return (
    <nav className={classes.nav}>
      <Link to='/' className={classes['logo-container']}>
        <img src={logo} alt='EmpireCraft' className={classes.logo} />
      </Link>
      <input
        type='checkbox'
        className={classes['nav__checkbox']}
        id='navi-toggle'
        ref={checkboxRef}
      />

      <label htmlFor='navi-toggle' className={classes['nav__button']}>
        <span className={classes['nav__icon']}>&nbsp;</span>
      </label>
      <ul className={classes['nav__list']}>
        <li>
          <NavigationDropdown title='Information'>
            <li>
              <Link to='/server-information' onClick={handleNavigatePage}>
                Server Information
              </Link>
            </li>
            <li>
              <Link to='/rules' onClick={handleNavigatePage}>
                Rules and Guidelines
              </Link>
            </li>
            <li>
              <Link to='/faq' onClick={handleNavigatePage}>
                FAQ
              </Link>
            </li>
            <li>
              <Link to='/plugins' onClick={handleNavigatePage}>
                Plugins and Datapacks
              </Link>
            </li>
            <li>
              <Link to='/world-downloads' onClick={handleNavigatePage}>
                World Downloads
              </Link>
            </li>
          </NavigationDropdown>
        </li>
        <li>
          <NavigationDropdown title='Community'>
            <li>
              <Link to='/leaderboard' onClick={handleNavigatePage}>
                Leaderboard
              </Link>
            </li>
            <li>
              <Link to='/calendar' onClick={handleNavigatePage}>
                Calendar
              </Link>
            </li>
            <li>
              <Link to='/content-creators' onClick={handleNavigatePage}>
                Content Creators
              </Link>
            </li>
          </NavigationDropdown>
        </li>
        <li>
          <NavigationDropdown title='Useful Links'>
            <li>
              <Link to='/support-server' onClick={handleNavigatePage}>
                Support the Server
              </Link>
            </li>
            <li>
              <a href='http://102.129.206.243:25566/'>Server Map</a>
            </li>
            <li>
              <Link to='/editor' onClick={handleNavigatePage}>
                Admin Tools
              </Link>
            </li>
          </NavigationDropdown>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
