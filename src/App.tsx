import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './pages/Login/Login';
import Navigation from './components/Navigation/Navigation';
import Calendar from './pages/Calendar/Calendar';
import Information from './pages/Information/Information';
import Leaderboard from './pages/Leaderboard/Leaderboard';
import WorldDownloads from './pages/WorldDownloads/WorldDownloads';
import Editor from './pages/Editor/Editor';
import ProtectedRoute from './helpers/ProtectedRoute';
import Plugins from './pages/Plugins/Plugins';
import Rules from './pages/Rules/Rules';
import FAQ from './pages/FAQ/FAQ';
import SupportServer from './pages/SupportServer/SupportServer';
import Home from './pages/Home/Home';
import ContentCreators from './pages/ContentCreators/ContentCreators';

const App: React.FC = (): JSX.Element => {
  return (
    <BrowserRouter>
      <Navigation />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/leaderboard' component={Leaderboard} />
        <Route path='/calendar' component={Calendar} />
        <Route path='/server-information' component={Information} />
        <Route path='/world-downloads' component={WorldDownloads} />
        <Route path='/login' component={Login} />
        <Route path='/rules' component={Rules} />
        <Route path='/plugins' component={Plugins} />
        <Route path='/faq' component={FAQ} />
        <Route path='/support-server' component={SupportServer} />
        <Route path='/content-creators' component={ContentCreators} />
        <ProtectedRoute path='/editor'>
          <Editor />
        </ProtectedRoute>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
