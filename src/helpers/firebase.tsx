import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyDUm8qO2tI5_y-hRFWL1zVjF90chZSVXyA',
  authDomain: 'empirecraft-d936c.firebaseapp.com',
  projectId: 'empirecraft-d936c',
  storageBucket: 'empirecraft-d936c.appspot.com',
  messagingSenderId: '460633756115',
  appId: '1:460633756115:web:044bdd64b484cf8cf39f26',
};

const configuredFirebase = firebase.initializeApp(firebaseConfig);

export default configuredFirebase;
