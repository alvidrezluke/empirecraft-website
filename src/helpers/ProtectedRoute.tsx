import { Redirect } from 'react-router-dom';
import firebase from './firebase';

interface IProtectedRouteOwnProps {
  path: string;
}

const ProtectedRoute: React.FC<IProtectedRouteOwnProps> = ({
  path,
  children,
}): JSX.Element => {
  return (
    <>
      {firebase.auth().currentUser ? (
        children
      ) : (
        <Redirect to={{ pathname: '/login', state: { origin: path } }} />
      )}
    </>
  );
};

export default ProtectedRoute;
