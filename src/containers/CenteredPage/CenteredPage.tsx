import classes from './CenteredPage.module.css';

interface ICenteredPageOwnProps {
  className?: string;
}

const CenteredPage: React.FC<ICenteredPageOwnProps> = ({
  children,
  className,
}): JSX.Element => {
  return (
    <div className={classes.center}>
      <main className={className ? className : ''}>{children}</main>
    </div>
  );
};

export default CenteredPage;
